content = {
	"Location": {
		"Name": "Engagis PH",
		"Code": "n/a",
		"Latitude": -33.865143,
		"Longitude": 151.2099,
		"TimeZoneId": "Taipei Standard Time",
		"Attributes": [
		{
			"Key": "EFM_MessageBankDownload",
			"Value": "False"
		},
		{
			"Key": "EFM_AutoSMS",
			"Value": "False"
		},
		{
			"Key": "StoreMobileNumber",
			"Value": "StoreMobileNumber"
		},
		{
			"Key": "AddressLine2",
			"Value": "AddressLine2"
		},
		{
			"Key": "State",
			"Value": "State"
		},
		{
			"Key": "AddressLine1",
			"Value": "AddressLine1"
		},
		{
			"Key": "Suburb",
			"Value": "Suburb"
		},
		{
			"Key": "PostCode",
			"Value": "PostCode"
		},
		{
			"Key": "PhoneNumber",
			"Value": "PhoneNumber"
		},
		{
			"Key": "EFM_WaitTimeExperience",
			"Value": "False"
		},
		{
			"Key": "Day1",
			"Value": "Monday - Thursday"
		},
		{
			"Key": "Time1",
			"Value": "9:30am - 4:00pm"
		},
		{
			"Key": "Day2",
			"Value": "Friday"
		},
		{
			"Key": "Time2",
			"Value": "9:30am - 5:00pm"
		},
		{
			"Key": "location_id",
			"Value": "location_id"
		},
		{
			"Key": "Store Type",
			"Value": "Store Type"
		},
		{
			"Key": "Store Name",
			"Value": "Store Name"
		}
		],
		"Id": 2631
	},
	"Attributes": [
	{
		"Key": "quividi_enabled",
		"Value": "0"
	},
	{
		"Key": "screen_display_times",
		"Value": "[{\"mon_on\":\"08:00\"},{\"mon_off\":\"17:30\"},{\"tue_on\":\"08:00\"},{\"tue_off\":\"17:30\"},{\"wed_on\":\"08:00\"},{\"wed_off\":\"17:30\"},{\"thu_on\":\"08:00\"},{\"thu_off\":\"17:30\"},{\"fri_on\":\"08:00\"},{\"fri_off\":\"17:30\"},{\"sat_on\":\"08:00\"},{\"sat_off\":\"17:30\"},{\"sun_on\":\"08:00\"},{\"sun_off\":\"17:30\"}]"
	},
	{
		"Key": "ezeconnect_link",
		"Value": ""
	},
	{
		"Key": "ei_log_level",
		"Value": "error"
	},
	{
		"Key": "device_electron_url",
		"Value": ""
	},
	{
		"Key": "device_electron_version",
		"Value": ""
	},
	{
		"Key": "device_electron_md5",
		"Value": ""
	},
	{
		"Key": "device_chromium_url",
		"Value": ""
	},
	{
		"Key": "device_chromium_version",
		"Value": ""
	},
	{
		"Key": "device_chromium_md5",
		"Value": ""
	},
	{
		"Key": "analytics_enabled",
		"Value": "0"
	}
	],
	"Applications": [
	{
		"Name": "Eze Suite",
		"Code": "ezesuite",
		"ParentCode": null,
		"ContentVersion": null,
		"AppVersion": null,
		"PayloadUrl": "",
		"Settings": [],
		"DataUpdatedAt": null
	},
	{
		"Name": "Eze Impress",
		"Code": "ezimp",
		"ParentCode": "ezesuite",
		"ContentVersion": "2020-12-14T03:43:33.793Z",
		"AppVersion": "6.13.1",
		"PayloadUrl": "",
		"Settings": [
		{
			"Key": "device_app_code",
			"Value": "ezimp"
		},
		{
			"Key": "device_app_entry",
			"Value": "/frontend/main/index.html"
		},
		{
			"Key": "device_app_md5",
			"Value": "d85751fbaead50abc6fab2fe5009fd0c"
		},
		{
			"Key": "device_app_url",
			"Value": "https://ezeimpress.s3-ap-southeast-2.amazonaws.com/ezeimpress6.13.1-68a370d.zip"
		},
		{
			"Key": "device_app_version",
			"Value": "6.13.1"
		},
		{
			"Key": "device_launch_type",
			"Value": "localweb"
		},
		{
			"Key": "device_ga_code",
			"Value": "UA-50517133-1"
		},
		{
			"Key": "device_chromium_url",
			"Value": "https://ezeconnect.s3-ap-southeast-2.amazonaws.com/chromium/chromium-68.0.3440.92.zip"
		},
		{
			"Key": "device_chromium_md5",
			"Value": "b838b5383ea450718bc77bacfa39558c"
		},
		{
			"Key": "device_chromium_version",
			"Value": "68.0.3440.92"
		},
		{
			"Key": "device_ezeconnect_link",
			"Value": "https://ezeconnect.s3-ap-southeast-2.amazonaws.com/self_update/ezimp/builddir/3.3.16/self_update.json"
		},
		{
			"Key": "device_electron_version",
			"Value": "1.1.4.0"
		},
		{
			"Key": "device_electron_url",
			"Value": "https://ezeconnect.s3-ap-southeast-2.amazonaws.com/renderer/electron-dir/ezeconnect-renderer-1.1.4.zip"
		},
		{
			"Key": "device_electron_md5",
			"Value": "30bf8b9d7d58491f6241718a72c1269f"
		}
		],
		"DataUpdatedAt": "2020-12-14T03:43:33.793Z"
	}
	],
	"DeviceCapabilities": [],
	"NFCCardGroups": [],
	"Ack": "success",
	"AckCode": 200,
	"Error": null,
	"Build": null,
	"Id": "3dd6e0c3-6503-4045-8d6e-70d1d9d654da",
	"Timestamp": "2020-12-14T07:26:54.7265484+00:00",
	"Hash": null
}