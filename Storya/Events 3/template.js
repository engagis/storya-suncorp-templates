function iterate(data, index) {
    $('#headline').css('visibility', 'hidden'); //Dirty but keeps the previous headline hidden while the current is loading it's font
    $('.highlight').css('visibility', 'hidden'); //Dirty but keeps the previous headline hidden while the current is loading it's font

    //NO REPEAT DATA ON LOOP
    let dataReference = sampleData[0].placeholders.map(_sample => { return { ..._sample, value: "", localPathValue: "" } });

    for (let i = 0; i < JSON.parse(JSON.stringify(dataReference)).length; i++) {
        if (data[index].placeholders.find(_dataRef => { return _dataRef.reference === dataReference[i].reference })) {
            dataReference[i] = data[index].placeholders.find(_dataRef => { return _dataRef.reference === dataReference[i].reference });
        }
    }

    data[index].placeholders = dataReference;

    //POPULATE DATA
    for (let i = 0; i < data[index].placeholders.length; i++) {
        if (data[index].placeholders[i].type === 'Text') {

            // Replace hard hyphen in message text AND style special characters
            // data[index].placeholders[i].value = data[index].placeholders[i].value.replace(/-/g, '\u2011') Not supported in Suncorp

            // Replace Storya Attribute Keys
            for (let j = 0; j < content.Location.Attributes.length; j++) {
                let attribute = content.Location.Attributes[j];
                data[index].placeholders[i].value = replaceAll(data[index].placeholders[i].value, `{{${attribute.Key}}}`, attribute.Value);
                data[index].placeholders[i].value = replaceAll(data[index].placeholders[i].value, `@{location.attribute['${attribute.Key}']}`, attribute.Value);
            }

            $(`#${data[index].placeholders[i].reference}`).text(data[index].placeholders[i].value)
        } else if (data[index].placeholders[i].type === 'Image') {
            $(`#${data[index].placeholders[i].reference}`).attr('src', data[index].placeholders[i].localPathValue)
        } else if (data[index].placeholders[i].type === 'Video') {
            $(`#${data[index].placeholders[i].reference}`).attr('src', data[index].placeholders[i].localPathValue)
        }
    }

    //LOOP TEMPLATE LOGIC STARTS HERE
    $('.container').fadeIn()
    $('.container').html($('.container').html());

    //Divide dynamic lines by line animation delay
    setTimeout(function () {
        separateByLine();
    }, 100);

    //Dynamic Animations
    $.keyframe.define([{
        name: 'headline_anim',
        '0%': { 'transform': 'translateX(-50vw)' },
        '100%': { 'transform': 'translateX(0)' }
    }, {
        name: 'msg_anim',
        '0%': { 'opacity': '0' },
        '100%': { 'opacity': '1' }
    }, {
        name: 'highlight_anim',
        '0%': {
            'transform': 'translateY(' + ($('#cta').height() + 10) + 'px)',
            'visibility': 'visible'
        },
        '100%': { 'transform': 'translateY(0)', 'visibility': 'visible' }
    }]);

    setTimeout(() => {
        $.keyframe.define([{
            name: 'highlight_anim',
            '0%': {
                'transform': 'translateY(' + ($('#cta').height() + 10) + 'px)',
                'visibility': 'visible'
            },
            '100%': { 'transform': 'translateY(0)', 'visibility': 'visible' }
        }]);
    }, 500)


    $(window).resize(function () {
        $.keyframe.define([{
            name: 'highlight_anim',
            '0%': {
                'transform': 'translateY(' + ($('#cta').height() + 10) + 'px)',
                'visibility': 'visible'
            },
            '100%': { 'transform': 'translateY(0)', 'visibility': 'visible' }
        }]);
    });

    setTimeout(() => {
        $('.container').fadeOut()
        if (data.length === index + 1) {
            try {
                callPostMessage()
                clearInterval(dataInterval)
            } catch {
                console.log('Preview Mode')
            }
        }
    }, 24500);
}

// This is the function the automation service will call to provide data
// DO NOT REMOVE OR RENAME THIS FUNCTION
function fillContent(data) {
    // TEMPLATE LOGIC STARTS HERE

    iterate(data, 0)
    var index = 1

    dataInterval = setInterval(function () {
        if (index >= data.length) {
            index = 0
        }
        iterate(data, index)
        index++;
    }, 25000);
    // TEMPLATE LOGIC ENDS HERE
}


function separateByLine() {
    var headline = $('#headline');
    var headlineWords = {};
    let lineHeight = 1;


    headline.each(function () {
        var words = $(this).text().split(' ');
        let line = words[0];

        $(this).text(words[0]);
        var height = $(this).height();

        if (words.length == 1) {
            headlineWords['span' + lineHeight] = words[0];
        } else {
            for (var i = 1; i < words.length; i++) {
                $(this).text($(this).text() + ' ' + words[i]);

                if ($(this).height() > height) {
                    height = $(this).height();

                    headlineWords['span' + lineHeight] = line;
                    line = words[i];
                    lineHeight++;
                } else {
                    line = line + " " + words[i];
                }
                if (words.length === i + 1) {
                    headlineWords['span' + lineHeight] = line;
                }
            }
        }
    });
    
    document.documentElement.style.setProperty('--delay', (lineHeight * 0.2) + "s");

    $('#headline').text("");
    $('#headline').css('visibility', 'visible');

    for (var key in headlineWords) {
        if (headlineWords.hasOwnProperty(key)) {
            $('#headline').append(`<span class="headline-anim ${key}">${headlineWords[key]} </span>`);
        }
    }

}

//"REPLACE()" ALL SUBSTITUTE to support chromium > 85
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
function replaceAll(str, match, replacement) {
    return str.replace(new RegExp(escapeRegExp(match), 'g'), () => replacement);
}