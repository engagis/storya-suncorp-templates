// SAMPLE DATA FOR TEMPLATE PREVIEW
sampleData = [
    {
        "placeholders": [
            {
                "type": "Image",
                "reference": "image",
                "localPathValue": "image.png"
            },
            {
                "type": "Text",
                "reference": "headline",
                "value": "Find out how to reach your goals sooner."
            },
            {
                "type": "Text",
                "reference": "description",
                "value": "Learn how to monitor your income vs. expenses and getting smart with your finances by joining this event."
            },
            {
                "type": "Text",
                "reference": "terms",
                "value": "We will be accommodating all customers on a first come first served basis. \nMeals are provided"
            },
            {
                "type": "Text",
                "reference": "location",
                "value": "Savings & Budgeting Workshop"
            },
            {
                "type": "Text",
                "reference": "date",
                "value": "Tuesday 14 January"
            },
            {
                "type": "Text",
                "reference": "time",
                "value": "No appointment necessary"
            },
            {
                "type": "Text",
                "reference": "cta",
                "value": "Speak to one of our friendly team about signing up today!"
            }
        ]
    }
]