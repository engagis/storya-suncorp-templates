// SAMPLE DATA FOR TEMPLATE PREVIEW
sampleData = [
    {
        "placeholders": [
            {
                "type": "Image",
                "reference": "photo",
                "localPathValue": "photo.png"
            },
            {
                "type": "Text",
                "reference": "headline",
                "value": "Meet the team"
            },
            {
                "type": "Text",
                "reference": "name",
                "value": "JESS WITNEY"
            },
            {
                "type": "Text",
                "reference": "position",
                "value": "STORE LEADER"
            },
            {
                "type": "Text",
                "reference": "number",
                "value": "+61 (0)000 000 000"
            }
        ]
    }
]