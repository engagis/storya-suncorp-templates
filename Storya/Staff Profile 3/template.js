function iterate(data, index) {

    //NO REPEAT DATA ON LOOP
    let dataReference = sampleData[0].placeholders.map(_sample => { return { ..._sample, value: "", localPathValue: "" } });

    for (let i = 0; i < JSON.parse(JSON.stringify(dataReference)).length; i++) {
        if (data[index].placeholders.find(_dataRef => { return _dataRef.reference === dataReference[i].reference })) {
            dataReference[i] = data[index].placeholders.find(_dataRef => { return _dataRef.reference === dataReference[i].reference });
        }
    }

    data[index].placeholders = dataReference;

    //POPULATE DATA
    for (let i = 0; i < data[index].placeholders.length; i++) {
        if (data[index].placeholders[i].type === 'Text') {

            // Replace hard hyphen in message text AND style special characters
            // data[index].placeholders[i].value = data[index].placeholders[i].value.replace(/-/g, '\u2011') Note supported in suncorp

            // Replace Storya Attribute Keys
            for (let j = 0; j < content.Location.Attributes.length; j++) {
                let attribute = content.Location.Attributes[j];
                data[index].placeholders[i].value = replaceAll(data[index].placeholders[i].value, `{{${attribute.Key}}}`, attribute.Value);
                data[index].placeholders[i].value = replaceAll(data[index].placeholders[i].value, `@{location.attribute['${attribute.Key}']}`, attribute.Value);
            }

            $(`#${data[index].placeholders[i].reference}`).text(data[index].placeholders[i].value)
        } else if (data[index].placeholders[i].type === 'Image') {
            $(`#${data[index].placeholders[i].reference}`).attr('src', data[index].placeholders[i].localPathValue)
        } else if (data[index].placeholders[i].type === 'Video') {
            $(`#${data[index].placeholders[i].reference}`).attr('src', data[index].placeholders[i].localPathValue)
        }
    }

    //LOOP TEMPLATE LOGIC STARTS HERE
    $('.container').fadeIn()
    $('.container').html($('.container').html());
    let curDate = new Date();
    let curDateMonth = curDate.toLocaleString('en-us', { month: 'long' }).toUpperCase();
    let curDateDate = curDate.getDate()

    //Dynamic photo height if portrait to aviod overlap
    $(window).resize(function () {
        calulateImageHeight();
    })

    setTimeout(function () {
        calulateImageHeight();
    }, 2000)

    function calulateImageHeight() {
        if ($('#photo').height() / $('#photo').width() > 960 / 1080) {
            $('#photo').addClass("height100");
        } else {
            $('#photo').addClass("width100");
        }
        if (window.innerHeight > window.innerWidth) {
            let msgBottom = $('.msg-container').offset().top + $('.msg-container').outerHeight(true);
            let imgTop = $('.img-container').offset().top;
            if (imgTop < msgBottom) {
                let newHeight = window.innerHeight - msgBottom;
                $('.img-container').css({ 'height': newHeight + 'px' });
            }
        } else { $('.img-container').css({ 'height': '100vh' }); }
    }

    //Divide dynamic line by line animation delay
    var headline = $('#headline');
    var headlineWords = {};
    let lineHeight = 1;

    if (!headline.text()) headline.text('Meet the team')

    headline.each(function () {
        var words = $(this).text().split(' ');
        let line = words[0];

        $(this).text(words[0]);
        var height = $(this).height();

        if (words.length == 1) {
            headlineWords['span' + lineHeight] = words[0];
        } else {
            for (var i = 1; i < words.length; i++) {
                $(this).text($(this).text() + ' ' + words[i]);

                if ($(this).height() > height) {
                    height = $(this).height();

                    headlineWords['span' + lineHeight] = line;
                    line = words[i];
                    lineHeight++;
                } else {
                    line = line + " " + words[i];
                }
                if (words.length === i + 1) {
                    headlineWords['span' + lineHeight] = line;
                }
            }
        }
    });

    var bodyStyles = window.getComputedStyle(document.body);
    var fooBar = bodyStyles.getPropertyValue('--delay');
    document.documentElement.style.setProperty('--delay', (lineHeight * 0.2) + "s");

    $('#headline').text("");

    for (var key in headlineWords) {
        if (headlineWords.hasOwnProperty(key)) {
            $('#headline').append(`<span class="headline-anim ${key}">${headlineWords[key]} </span>`);
        }
    }

    //Dynamic Animations
    $.keyframe.define([{
        name: 'headline_anim',
        '0%': { 'transform': 'translatex(-50vw)' },
        '100%': { 'transform': 'translatex(0)' }
    }, {
        name: 'name_anim',
        '0%': { 'transform': 'translateY(' + $('#name').height() + 'px)' },
        '100%': { 'transform': 'translateY(0)' }
    }, {
        name: 'position_anim',
        '0%': { 'transform': 'translateY(' + $('#position').height() + 'px)' },
        '100%': { 'transform': 'translateY(0)' }
    }, {
        name: 'number_anim',
        '0%': { 'transform': 'translateY(' + $('#transform').height() + 'px)' },
        '100%': { 'transform': 'translateY(0)' }
    }]);

    setTimeout(() => {
        $('.container').fadeOut()
        if (data.length === index + 1) {
            try {
                callPostMessage()
                clearInterval(dataInterval)
            } catch {
                console.log('Preview Mode')
            }
        }
    }, 24500);
}

// This is the function the automation service will call to provide data
// DO NOT REMOVE OR RENAME THIS FUNCTION
function fillContent(data) {
    // TEMPLATE LOGIC STARTS HERE

    iterate(data, 0)
    var index = 1

    dataInterval = setInterval(function () {
        if (index >= data.length) {
            index = 0
        }
        iterate(data, index)
        index++;
    }, 25000);
    // TEMPLATE LOGIC ENDS HERE
}

//"REPLACE()" ALL SUBSTITUTE to support chromium > 85
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
function replaceAll(str, match, replacement) {
    return str.replace(new RegExp(escapeRegExp(match), 'g'), () => replacement);
}