function iterate(data, index) {
    $('#headline').css('visibility', 'hidden'); //Dirty but keeps the previous headline hidden while the current is loading it's font

    //NO REPEAT DATA ON LOOP
    let dataReference = sampleData[0].placeholders.map(_sample => { return { ..._sample, value: "", localPathValue: "" } });

    for (let i = 0; i < JSON.parse(JSON.stringify(dataReference)).length; i++) {
        if (data[index].placeholders.find(_dataRef => { return _dataRef.reference === dataReference[i].reference })) {
            dataReference[i] = data[index].placeholders.find(_dataRef => { return _dataRef.reference === dataReference[i].reference });
        }
    }

    data[index].placeholders = dataReference;

    //LOOP TEMPLATE LOGIC STARTS HERE
    $('.container').fadeIn()
    $('.container').html($('.container').html());

    //POPULATE DATA
    for (let i = 0; i < data[index].placeholders.length; i++) {
        if (data[index].placeholders[i].type === 'Text') {

            // Replace hard hyphen in message text AND style special characters
            data[index].placeholders[i].value = data[index].placeholders[i].value.replace(/-/g, '\u2011')

            // Replace Storya Attribute Keys
            for (let j = 0; j < content.Location.Attributes.length; j++) {
                let attribute = content.Location.Attributes[j];
                data[index].placeholders[i].value = replaceAll(data[index].placeholders[i].value, `{{${attribute.Key}}}`, attribute.Value);
                data[index].placeholders[i].value = replaceAll(data[index].placeholders[i].value, `@{location.attribute['${attribute.Key}']}`, attribute.Value);
            }

            $(`#${data[index].placeholders[i].reference}`).text(data[index].placeholders[i].value)
        } else if (data[index].placeholders[i].type === 'Image') {
            $(`#${data[index].placeholders[i].reference}`).attr('src', data[index].placeholders[i].localPathValue)
        } else if (data[index].placeholders[i].type === 'Video') {
            $(`#${data[index].placeholders[i].reference}`).attr('src', data[index].placeholders[i].localPathValue)
        }
    }

    //Weather Widget
    weatherWidget();

    //Divide dynamic lines by line animation delay
    setTimeout(function () {
        separateByLine();
    }, 100);

    setTimeout(() => {
        $('.container').fadeOut()
        if (data.length === index + 1) {
            try {
                callPostMessage()
                clearInterval(dataInterval)
            } catch {
                console.log('Preview Mode')
            }
        }
    }, 24500);
}

// This is the function the automation service will call to provide data
// DO NOT REMOVE OR RENAME THIS FUNCTION
function fillContent(data) {
    // TEMPLATE LOGIC STARTS HERE

    iterate(data, 0)
    var index = 1

    dataInterval = setInterval(function () {
        if (index >= data.length) {
            index = 0
        }
        iterate(data, index)
        index++;
    }, 25000);
    // TEMPLATE LOGIC ENDS HERE
}

function separateByLine(suburb) {
    var headline = $('#headline');
    var headlineWords = {};
    let lineHeight = 1;

    headline.each(function () {
        var words = $(this).text().split(' ');

        let line = words[0];

        $(this).text(words[0]);
        let height = $(this).height();

        if (words.length == 1) {
            if (words[0] == 'Suburb' || words[0] == 'StoreCode') headlineWords['span' + (lineHeight + 1)] = 'Suncorp';
            else if (words[0] == '') headlineWords['span' + (lineHeight + 1)] = 'Suncorp'
            else headlineWords['span' + (lineHeight + 1)] = words[0];
        } else {
            for (var i = 1; i < words.length; i++) {
                $(this).text($(this).text() + ' ' + words[i]);

                if ($(this).height() > height) {
                    height = $(this).height();

                    headlineWords['span' + (lineHeight + 1)] = line;
                    line = words[i];
                    lineHeight++;
                } else if (i != words.length) {
                    line = line + " " + words[i];
                }
                if (words.length === i + 1) {
                    headlineWords['span' + (lineHeight + 1)] = line;
                }
            }
        }
    });

    var bodyStyles = window.getComputedStyle(document.body);
    var fooBar = bodyStyles.getPropertyValue('--delay');
    document.documentElement.style.setProperty('--delay', (lineHeight * 0.2) + "s");

    $('#headline').text("");
    $('#headline').css('visibility', 'visible');

    $('#headline').append(`<span class="headline-anim span1">Welcome to </span>`);
    for (var key in headlineWords) {
        if (headlineWords.hasOwnProperty(key)) {
            $('#headline').append(`<span class="headline-anim ${key}">${headlineWords[key]} </span>`);
        }
    }


}

function weatherWidget() {
    $('.widget-container').hide();
    try {
        let code_id = content.Location.Attributes.find(attr => attr.Key === "PostCode").Value;
        // let code_id = '5000'
        let id = code_id;

        if (id && !isNaN(id)) {
            // id = result.find(_attrib => _attrib.name === locationCode).id;
            $.getJSON(`http://fetcher.engagiscreatives.com.au/index.php?feed=2&param=${id}`, function (result) {
                let temp = Math.round(result.list[0].main.temp * 10) / 10;
                if (result.cod !== '200') { console.log('Weather details not found'); }
                else {
                    $('.widget-placeholder').hide();
                    $('.widget-container').show();
                    var weathCond = result.list[0].weather[0].main;
                    //var weathCond = "Snow"; // Uncomment and modify to test icons
                    if (weathCond == 'Clouds') {
                        $('#widget-img').attr('src', 'icon_clouds.svg');
                    } else if (weathCond == 'Clear') {
                        $('#widget-img').attr('src', 'icon_sunny.svg');
                    } else if (weathCond == 'Rain') {
                        $('#widget-img').attr('src', 'icon_rain.svg');
                    } else if (weathCond == 'Snow') {
                        $('#widget-img').attr('src', 'icon_snow.svg');
                    }
                    $('#widget-text').html(`${temp}­°C`);
                }
            }).fail(() => { console.log('Weather details not found'); })
        } else { console.error('PostCode empty or not a Number'); }
    } catch (error) {
        console.error('PostCode not found');
    }
}

//"REPLACE()" ALL SUBSTITUTE to support chromium > 85
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
function replaceAll(str, match, replacement) {
    return str.replace(new RegExp(escapeRegExp(match), 'g'), () => replacement);
}