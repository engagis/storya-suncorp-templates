var engagisEvent;

window.addEventListener('message', receiveMessage, false);

function callPostMessage(){
    engagisEvent.source.postMessage('engagis.complete', engagisEvent.origin);
}

function receiveMessage(event) {
    if (event.data.event && event.data.event == 'engagis.display') {
        engagisEvent = event;
        if (skipNow) {
            engagisEvent.source.postMessage('engagis.complete', engagisEvent.origin);
        }
    }
}

function showtime(data) {
    if (data) {
        $("#statusholder").append('<div id="ready"></div>')
        showing = true;
        fillContent(data)
        setTimeout(() => { $('#mb1').fadeOut() }, 200);
    }
}

async function initiate() {

    fetch("data.json")
      .then(r => r.json())
      .then(async function(json) {
          console.log('data.json: ', json);
          json.slides = json.slides.sort(function(a, b){
              if(a.order < b.order) { return -1; }
              if(a.order > b.order) { return 1; }
              return 0;
          });

        if (json) {
            showtime(json.slides);
        } else {
            showtime(sampleData)
        }
      })
      .catch(error => {
          console.log('Displaying sampleData');
          showtime(sampleData)
      });
}
