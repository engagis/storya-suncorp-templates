function updateTime() {
    // var days = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
    let days = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    let months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

    let now = new Date();

    let time = now.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    $('#time').text(time)

    let day = days[now.getDay()] + ", " + now.getDate() + " " + months[now.getMonth()];
    $('#date').text(day)
}

function updateWeatherWidget() {
    try {
        let id = content.Location.Attributes.find(attr => attr.Key === "PostCode").Value;
        // let id = 5000;
        if (id && !isNaN(id)) {
            // id = result.find(_attrib => _attrib.name === locationCode).id;
            $.getJSON(`http://fetcher.engagiscreatives.com.au/index.php?feed=2&param=${id}`, (result) => {
                let tempMax = Math.round(result.list[0].main.temp_max * 10) / 10;
                let tempMin = Math.round(result.list[0].main.temp_min * 10) / 10;

                if (result.cod !== '200') { console.log('Weather details not found'); }
                else {
                    $('.widget-weather-container').css('display', 'flex');
                    var weathCond = result.list[0].weather[0].main;
                    //var weathCond = "Snow"; // Uncomment and modify to test icons
                    if (weathCond == 'Clouds') {
                        $('#today-img').attr('src', 'assets/icon_clouds.svg');
                    } else if (weathCond == 'Clear') {
                        $('#today-img').attr('src', 'assets/icon_sunny.svg');
                    } else if (weathCond == 'Rain') {
                        $('#today-img').attr('src', 'assets/icon_rain.svg');
                    } else if (weathCond == 'Snow') {
                        $('#today-img').attr('src', 'assets/icon_snow.svg');
                    }
                    $('#today-max').html(`${tempMax}­°C`);
                    $('#today-min').html(`${tempMin}­°C`);

                    let tempMax8 = Math.round(result.list[8].main.temp_max * 10) / 10;
                    let tempMin8 = Math.round(result.list[8].main.temp_min * 10) / 10;
                    $('.widget-placeholder').hide();
                    $('.widget-container').show();
                    var weathCond = result.list[8].weather[0].main;
                    //var weathCond = "Snow"; // Uncomment and modify to test icons
                    if (weathCond == 'Clouds') {
                        $('#tomorrow-img').attr('src', 'assets/icon_clouds.svg');
                    } else if (weathCond == 'Clear') {
                        $('#tomorrow-img').attr('src', 'assets/icon_sunny.svg');
                    } else if (weathCond == 'Rain') {
                        $('#tomorrow-img').attr('src', 'assets/icon_rain.svg');
                    } else if (weathCond == 'Snow') {
                        $('#tomorrow-img').attr('src', 'assets/icon_snow.svg');
                    }
                    $('#tomorrow-max').html(`${tempMax8}­°C`);
                    $('#tomorrow-min').html(`${tempMin8}­°C`);
                }
            }).fail(() => { console.log('Tomorrow weather details not found'); })
        } else { console.error('PostCode empty or not a Number'); }
    } catch (error) {
        console.error('PostCode not found');
    }
}

function getNewsDetails() {


    $.ajax({
        type: "GET",
        // url: "abc.xml",
        // url: "http://www.abc.net.au/news/feed/2942460/rss.xml",
        url: "https://rss.engagisdemo.com.au/suncorp-news",
        dataType: "xml",
        success: populateNews
    }).fail((err) => {
        startCarousel()
    });


    // $.get("http://fetcher.engagiscreatives.com.au/index.php?feed=3&param=2157142",(data) => {
    //     populateNews(data);
    // }).fail(function(jqXHR, textStatus, errorThrown) {
    //     console.log(textStatus );
    // });

}

function populateNews(datas) {

    /*
    console.log(datas.news[0]);

    // RESET CAROUSEL
    $('#carousel').text('');
    console.log($('#carousel'));

    // POPULATE CAROUSEL
    for (let news of datas.news) {
        $('#carousel').append(`
        <div class="carouse-item">
                <img class="image" src="${news.image}" />
                <div class="headline">${news.title}</div>
                <div class="subheadline">${news.description}</div>
            </div>
            `);
    }
    */


    // POPULATE TITLE
    $('#title').text(datas.getElementsByTagName("channel")[0].getElementsByTagName("title")[0].childNodes[0].nodeValue)


    // RESET CAROUSEL
    $('.body-container').html(`<div id="carousel"></div>`)

    // POPULATE CAROUSEL
    let items = datas.getElementsByTagName("item");
    for (let item of items) {
        if (window.innerHeight < window.innerWidth) {
            $('#carousel').append(`
            <div class="carouse-item">
                <div class="item-container">
                    <img class="image" src="${item.getElementsByTagName("media:content")[0].getAttribute('url')}" />
                    <div class="message-container">
                    <div class="headline">${item.getElementsByTagName("title")[0].childNodes[0].nodeValue}</div>
                    <div class="subheadline">${item.getElementsByTagName("description")[0].childNodes[0].wholeText}</div>
                    </div>
                    </div>
                </div>
                `);
        } else {
            $('#carousel').append(`
            <div class="carouse-item">
                    <img class="image" src="${item.getElementsByTagName("media:content")[0].getAttribute('url')}" />
                    <div class="headline">${item.getElementsByTagName("title")[0].childNodes[0].nodeValue}</div>
                    <div class="subheadline">${item.getElementsByTagName("description")[0].childNodes[0].wholeText}</div>
                </div>
                `);
        }
    }


    // START CAROUSEL AFTER POPULATING NEWS
    startCarousel()
}

function startCarousel() {
    $('#carousel').not('.slick-initialized').slick({
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 10000,
        infinite: true,
    });
}

$(() => {
    //Update Time Widget
    setInterval(updateTime, 1000);

    //Update News
    getNewsDetails()

    //Update weather
    updateWeatherWidget()

    window.addEventListener('resize', getNewsDetails);
})