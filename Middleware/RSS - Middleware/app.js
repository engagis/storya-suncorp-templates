'use strict';

const { http, https } = require('follow-redirects');
const fs = require('fs');
const schedule = require('node-schedule');

console.log('trying to start server at port 3000...');
let server = http.createServer(function (req, res) {

    if (req.url == '/suncorp-news' && req.method == 'GET') {
        const data = fs.readFileSync('data.xml');
        
        res.writeHead(200, {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Content-Type': 'xml'
        });
        
        res.end(data);
    } else {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        let message = 'You\'ve reached the Engagis RSS Source!\n',
            version = 'NodeJS ' + process.versions.node + '\n',
            response = [message, version].join('\n');
        res.end(response);
    }
    
}).listen(3000);

server.on('error', function (err) {
    // Handle your error here
    console.log(err);
});


console.log("Scheduled to update News every hour is set...");
updateNews()
schedule.scheduleJob('0 * * * *', function () {
    updateNews()
});

function updateNews() {
    console.log("Updating News...");
    // http.get('http://localhost/Gitlab/ezeimpress-templates-suncorp/RSS/abc.xml', (res) => {
        http.get('http://www.abc.net.au/news/feed/2942460/rss.xml', (res) => {
        const { statusCode } = res;
        const contentType = res.headers['content-type'];


        // Any 2xx status code signals a successful response but
        // here we're only checking for 200.
        if (statusCode !== 200) {
            let error = new Error('Request Failed.\n' + `Status Code: ${statusCode}`);
            console.error(error.message);
            // Consume response data to free up memory
            res.resume();
            return;
        }

        res.setEncoding('utf8');
        let data = '';
        res.on('data', (chunk) => { data += chunk; });
        res.on('end', () => {
            try {
                fs.writeFile('./data.xml', data, function (err) {
                    if (err) throw err;
                    console.log('News Data Updated!');
                });
            } catch (e) {
                console.error(e.message);
            }
        });
    })
}